
使用eclipse提交代码到oschina简要说明：

1·尽量保证eclipse的项目名称跟oschina上托管的项目名称一致；

2·创建项目后，选中导航：管理->部署公钥管理-选中右边的启用（公钥）；

3·把在oschina创建好的项目clone到本地，操作步骤：a.创建一个文件夹把（id_rsa/id_rsa_pub）复制到该新建的文件夹内；b.用git clone oschina上刚创建的项目到该文件夹，命令为：cd D:/xx

$ git clone http://git.oschina/net/新建的项目名

这样就可以把在oschina 上创建的项目clone到本地了；

4·把在eclipse上新建的项目复制到clone后的文件夹内；

5·在eclipse中Package Explorer中右键 Import->Git->Local->add到上条生成的Android项目，导入到eclipse就可以了；

6·选择导入进来的项目 Team->Commit->Push to upstream